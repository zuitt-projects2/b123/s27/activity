let http = require('http');

let courses = [
	
	{
		name: "MATH 123",
		description: "basic math",
		price: "2000",
		isActive:true
	},
	{
		name: "SCIENCE 123",
		description: "basic science",
		price: "1500",
		isActive:true
	},
	{
		name: "ENGLISH 123",
		description: "basic english",
		price: "1300",
		isActive:true
	},
	

]

http.createServer(function(request,response){

	if (request.url === "/courses" && request.method === "GET"){
		response.writeHead(200,{'Content-Type':'application/json'})
		response.end(JSON.stringify(courses))
	
	}  else if(request.url === "/courses" && request.method === "POST") {
		let requestBody = "";
		request.on('data',function(data) {
			/*console.log(data);*/
			requestBody += data;
		})
		request.on('end',function(){
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let newCourse = {

				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price,
				isActive : true
			}

			courses.push(newCourse);
			console.log(courses)
			response.writeHead(200,{'Content-Type':'application/json'});
			response.end(JSON.stringify(`Course ${newCourse.name} added`));

		})

	} else if(request.url ==="/courses/getSingleCourse" && request.method === "POST"){

		let requestBody = "";
		request.on('data',function(data){
			requestBody += data;
		})
		request.on('end',function(data) {
			requestBody = JSON.parse(requestBody);

			let foundCourse = courses.find(function(course){
				return course.name === requestBody.name
			})
			console.log(foundCourse)
			if (foundCourse !== undefined ) {
				response.writeHead(200,{'Content-Type':'application/json'})
				response.end(JSON.stringify(foundCourse))
			} else { 
				response.writeHead(404,{'Content-Type':'text/plain'});
				response.end(`Course Not Found`)
			}
			
		})

	} else if(request.url ==="/courses/deleteSingleCourse" && request.method ==="DELETE"){

		courses.pop()
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end("Course deleted")


	} else { 
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end(`Resource not Found`)}



}).listen(8000)

console.log(`Server is running at port 8000`)

